import React, {Component} from 'react';
import './App.css';
import TaskForm from "./components/AddTaskForm/AddTaskForm";
import Task from "./components/Tasks/Tasks";

class App extends Component {
    state = {
        currentId: 3,
        tasks: [
            {task: 'buy Milk',  id: 1},
            {task: 'go to shop', id: 2},
            {task: 'call to mummy',  id: 3}
        ],
        showTasks: false,
        value: ''
    };

    changeHandler = value => {
        this.setState({value})
    };
    removeHadler = id =>{
        const tasks = [...this.state.tasks];
        const taskId = tasks.findIndex(task => task.id === id);
        tasks.splice(taskId, 1);
        this.setState({tasks});
    };

    addHandler = () => {
        const newTask = {task: this.state.value, id: this.state.currentId + 1};
        let tasks = [...this.state.tasks, newTask];
        this.setState({tasks, currentId: this.state.currentId + 1, value: ""})
    };



    render(){
       return (
           <div className="App">
               <TaskForm addHandler={this.addHandler} value={this.state.value} changeHandler={this.changeHandler}/>
               {this.state.tasks.map((task, key) => <Task remove={()=> this.removeHadler(task.id)} task={task.task} key={key}/>)}
           </div>
       )

    }
}

export default App;