import React from 'react';
import './task.css';

const  Task = props => (
    <div className="Task">
         <p>{props.task}</p>
        <button onClick={props.remove}>X</button>
    </div>

);
export default Task;